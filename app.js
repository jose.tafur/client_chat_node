const express = require('express');

const app = express();

// const ActionCable = require('action-cable-node');
//
// const App = {}
// App.cable = ActionCable.createConsumer()
// App.cable.subscriptions.create({ channel: "HelloChannel", room: "Best room!"})

ActionCable = require('actioncable')

var cable = ActionCable.createConsumer('wss://localhost:3000/cable')

cable.subscriptions.create('HelloChannel', {
  // normal channel code goes here...
});

app.listen('8080',function(){
  console.log('RUN SERVER');
})
