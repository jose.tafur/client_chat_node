let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);


io.on('connection', (socket) => {

  socket.on('disconnect', function(){
    io.emit('users-changed', {user: socket.nickname, event: 'left'});
  });

  socket.on('set-nickname', (nickname) => {
    console.log(nickname)
    socket.nickname = nickname;
    io.emit('users-changed', {user: nickname, event: 'joined'});
  });

  socket.on('add-message', (message) => {
    io.emit('message', {text: message.text, from: socket.nickname, created: new Date()});
  });
});

var port = process.env.PORT || 8080;

app.get('/pendientes',function(req,res){
  io.sockets.emit('set-nickname', 'hola');
  res.send('hola')
})

http.listen(port, function(){
   console.log('listening in http://localhost:' + port);
});
