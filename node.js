var App = {};

App.sendmessage = function(send) {
  data = {
    message : send,
    action : "speak"
  };
  message = {
    command: "message",
    identifier: JSON.stringify(App.param),
    data: JSON.stringify(data)
  };
  App.ws.send(JSON.stringify(message));
}

App.connect_server = function() {
  const WebSocket = require('ws');
  App.ws = new WebSocket('ws://0.0.0.0:3001/cable', ["actioncable-v1-json", "actioncable-unsupported"]);

  App.param = {channel: "SocketConnectionChannel", uuid: guid()};

  App.ws.on('open', function open() {
    data = {
      command: "subscribe",
      identifier: JSON.stringify(App.param)
    }
    App.ws.send(JSON.stringify(data));
  });
  App.ws.on('message', function (event) {
    console.log(event);
  });
  function guid() {
   function s4() {
     return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
   }
   return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
  }
}
App.connect_server();

App.sendmessage("Hello world");
